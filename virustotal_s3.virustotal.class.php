<?php
/*
 * YodifyVirusTotal extends VirusTotal
 * Purpose: Have the system use our local cache to look up the SHA256 code for the requested file.
 */
define('YODIFY_FILE_NOT_SCANNED', -1);
define('YODIFY_FILE_PENDING', 0);
define('YODIFY_FILE_OK', 1);
define('YODIFY_FILE_BAD', 2);

define('YODIFY_URL_SCAN', 0);
define('YODIFY_FILE_SCAN', 1);

class YodifyVirusTotal extends VirusTotal{
  public function __construct($api_key = NULL) {
    parent::__construct($api_key);
  }
  
  protected function get_file_info($file){
    return db_select('virustotal_s3', 's')
      ->condition('s.fid', $file->fid)
      ->fields('s', array('scan_type','scan_id','status','last_update'))
      ->range(0, 1)
      ->execute()->fetch();
    
  }
  
  protected function setFileStatus(&$file, $status){
    if ($status == YODIFY_FILE_OK){
      $flag = flag_get_flag('yodify_ok_file');
      $flag->flag('flag', $file->fid);
    }else{
      $flag = flag_get_flag('yodify_bad_file');
      $flag->flag('flag', $file->fid);
    }
    db_update('virustotal_s3')
      ->condition('fid', $file->fid)
      ->fields(array(
        'status' => $status,
        'last_update' => time(),
      ))->execute();
  }
  
  public function getFileStatus(&$file) {
    $file_info = $this->get_file_info($file);
    if (!isset($file_info->scan_id)) return YODIFY_FILE_NOT_SCANNED;
    $scan_id = $file_info->scan_id;
    $status = $file_info->status;
    if ($status == YODIFY_FILE_PENDING){
      $response = $this->getScanReport($file, $file_info);
      if (isset($response['positives'])){//scan is done
        $ratio = $response['positives']/$response['total'];
        $status = $ratio<.05?YODIFY_FILE_OK:YODIFY_FILE_BAD;//less than 5% bad is ok; accounts for false positives.
        $this->setFileStatus($file, $status);
      }
    }
    return $status;
  }
  
  public function getScanReport(&$file, $file_info = null) {
    if (empty($file_info)) $file_info = $this->get_file_info($file);//get the file info
    if (!empty($file_info->scan_id)){
      if ($file_info->scan_type == YODIFY_URL_SCAN){
        $response = $this->apiRequest(VIRUSTOTAL_FUNC_GET_URL_REPORT, array('resource' => $file_info->scan_id));
        if (isset($response['filescan_id'])){//Then a file scan was done, and we can check that instead.
          db_update('virustotal_s3')
            ->condition('fid', $file->fid)
            ->fields(array(
              'scan_type' => YODIFY_FILE_SCAN,
              'scan_id' => $response['filescan_id'],
              'last_update' => time(),
            ))->execute();
          //A file scan was done, so use that report instead:
          return $this->apiRequest(VIRUSTOTAL_FUNC_GET_FILE_REPORT, array('resource' => $response['filescan_id']));
        }else{
          return $response;//if no file scan was done, just use the URL report.
        }
      }else{//Then it must be a file scan:
        return $this->apiRequest(VIRUSTOTAL_FUNC_GET_FILE_REPORT, array('resource' => $file_info->scan_id));
      }
    }
    return FALSE;
  }
  
  public function scanFile($file) {
    if (!is_object($file)) return FALSE;
    //d($file, '$file');

    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    if (!$wrapper) return FALSE;
    
    if ($real_path = $wrapper->realpath()){//real_path is the path in the local filesystem (not an external url).
      $scan_type = YODIFY_FILE_SCAN;
      $response = $this->apiRequest(VIRUSTOTAL_FUNC_SCAN_FILE, array('file' => '@' . $real_path . ';type=' . $file->filemime . ';filename=' . $file->filename) );
    }else if (strpos($file->uri, 's3://') === 0){//S3 doesn't have a real_path since the file isn't local.
      $scan_type = YODIFY_URL_SCAN;
      $bucket = variable_get('amazons3_bucket', '');
      $local_path  = trim(str_replace('s3://', '', $file->uri), '/');
      $timeout = time() + 60*60*24*7;//Link should work for a week...
      $url = $this->getS3()->get_object_url($bucket, $local_path, $timeout, array('torrent' => false, 'response' => array()));
      $response = $this->apiRequest(VIRUSTOTAL_FUNC_SCAN_URL, array('url' => $url));
    }
    //d($response, '$response from scanFile');
    if ($response && isset($response['scan_id'])){
      db_insert('virustotal_s3')
      ->fields(array(
        'fid' => $file->fid,
        'scan_type' => $scan_type,
        'scan_id' => $response['scan_id'],
        'last_update' => REQUEST_TIME,
      ))
      ->execute();
      $flag = flag_get_flag('yodify_scanned');
      $flag->flag('flag', $file->fid);
    }else{
      watchdog('yodify', 'File failed to scan: ' . $file->filename . '(fid:' . $file->fid . ')', null, WATCHDOG_ERROR);
    }
    return $response;
  }
  
  //This is taken from AmazonS3StreamWrapper.inc
  protected function getS3() {
    if($this->s3 == null) {
      $bucket = variable_get('amazons3_bucket', '');

      if(!libraries_load('awssdk') && !isset($bucket)) {
        drupal_set_message('Unable to load the AWS SDK. Please check you have installed the library correctly and configured your S3 credentials.'. 'error');
      }
      else if(!isset($bucket)) {
        drupal_set_message('Bucket name not configured.'. 'error');
      }
      else {
        try {
         $this->s3 = new AmazonS3();
         $this->bucket = $bucket;
        }
        catch(RequestCore_Exception $e){
          drupal_set_message('There was a problem connecting to S3', 'error');
        }
        catch(Exception $e) {
          drupal_set_message('There was a problem using S3: ' . $e->getMessage(), 'error');
        }
      }
    }
    return $this->s3;
  }
  
  /*
   * Stole this out of the virustotal.class.php file. Comment out the drupal_alter code
   *  since it automatically creates status messages on the page, which sucks.
   */
  protected function apiRequest($function, $data = array()) {

    // Allow modules to make changes on post data.
    //drupal_alter('virustotal_query', $data, $function);

    // Initialise cURL.
    $curl_handle = curl_init($this->apiUrl . $function);

    $curl_options = array(
      // Set POST data.
      CURLOPT_POST => TRUE,
      // Add key to HTTP POST data.
      CURLOPT_POSTFIELDS => $data + array('apikey' => $this->apiKey),
      // Results will be returned and not displayed.
      CURLOPT_RETURNTRANSFER => TRUE,
      // Forbid reuse so no quota can be wasted.
      CURLOPT_FORBID_REUSE => TRUE,
      // This is a fix for the Curl library to prevent
      // Expect: 100-continue headers in POST requests.
      CURLOPT_HTTPHEADER => array('Expect:'),
    );

    // Set cURL options.
    curl_setopt_array($curl_handle, $curl_options);

    // Send the HTTP POST request with cURL.
    $json_data = curl_exec($curl_handle);

    // Check for cURL errors.
    if (curl_errno($curl_handle)) {
      $result_data = array('response_code' => VIRUSTOTAL_RESULT_HTTP_ERROR);
    }
    else {
      // Decode returned json data to an array.
      $result_data = drupal_json_decode($json_data);
    }

    // Close cURL handle.
    curl_close($curl_handle);

    // Log the result by watchdog.
    watchdog('virustotal', check_plain(@$result_data['verbose_msg']));

    // Allow modules to react on query result.
    //drupal_alter('virustotal_result', $result_data, $function);

    return $result_data;
  }
}